<?php

    // Header Slider
    add_action( 'init', 'cpt_slider' );
    function cpt_slider() {
        $labels = array(
            'name'               => __( 'Header Slider' ),
            'singular_name'      => __( 'Header Slider' ),
            'menu_name'          => __( 'Header Slider' ),
            'all_items'          => __( 'All slides' ),
            'insert_into_item'   => __( 'Insertar en slide' ),
            'add_new'            => __( 'Añadir nueva slide' ),
        );
        $args = array(
            'labels'             => $labels,
            'supports'           => array( 'title' ),
            'public'             => true,
            'has_archive'        => true,
            'menu_position'      => 5,
            'menu_icon'          => 'dashicons-images-alt2',
            'rewrite'            => array( 'slug' => 'header-slider' )
        );
        register_post_type( 'ot_header_slider', $args );
    };

    // Values
    add_action( 'init', 'cpt_values' );
    function cpt_values() {
        $labels = array(
            'name'               => __( 'Valores' ),
            'singular_name'      => __( 'Valores' ),
            'menu_name'          => __( 'Valores' ),
            'all_items'          => __( 'Todos los valores' ),
            'insert_into_item'   => __( 'Insertar en item' ),
            'add_new'            => __( 'Añadir nuevo valor' ),
        );
        $args = array(
            'labels'             => $labels,
            'supports'           => array( 'title', 'editor', 'thumbnail' ),
            'public'             => true,
            'has_archive'        => true,
            'menu_position'      => 5,
            'menu_icon'          => 'dashicons-star-filled',
            'rewrite'            => array( 'slug' => 'valores' )
        );
        register_post_type( 'ot_values', $args );
    };

    // Team
    add_action( 'init', 'cpt_team' );
    function cpt_team() {
        $labels = array(
            'name'               => __( 'Equipo' ),
            'singular_name'      => __( 'Equipo' ),
            'menu_name'          => __( 'Equipo' ),
            'all_items'          => __( 'Todos los integrantes' ),
            'insert_into_item'   => __( 'Insertar en integrante' ),
            'add_new'            => __( 'Añadir nuevo integrante' ),
        );
        $args = array(
            'labels'             => $labels,
            'supports'           => array( 'title', 'editor', 'thumbnail' ),
            'public'             => true,
            'has_archive'        => true,
            'menu_position'      => 5,
            'menu_icon'          => 'dashicons-admin-users',
            'rewrite'            => array( 'slug' => 'equipo' )
        );
        register_post_type( 'ot_team', $args );
    };

    // Services
    add_action( 'init', 'cpt_services' );
    function cpt_services() {
        $labels = array(
            'name'               => __( 'Services' ),
            'singular_name'      => __( 'Service' ),
            'menu_name'          => __( 'Services' ),
            'all_items'          => __( 'Todos los servicios' ),
            'insert_into_item'   => __( 'Insertar en servicio' ),
            'add_new'            => __( 'Añadir nuevo servicio' ),
        );
        $args = array(
            'labels'             => $labels,
            'supports'           => array( 'title' ),
            'public'             => true,
            'has_archive'        => true,
            'menu_position'      => 5,
            'menu_icon'          => 'dashicons-editor-ul',
            'rewrite'            => array( 'slug' => 'servicio' ),
            'taxonomies'         => array( 'category' )
        );
        register_post_type( 'ot_services', $args );
    };

    // Industries
    add_action( 'init', 'cpt_industries' );
    function cpt_industries() {
        $labels = array(
            'name'               => __( 'Industries' ),
            'singular_name'      => __( 'Industry' ),
            'menu_name'          => __( 'Industries' ),
            'all_items'          => __( 'Todas las industrias' ),
            'insert_into_item'   => __( 'Insertar en industria' ),
            'add_new'            => __( 'Añadir nueva industria' ),
        );
        $args = array(
            'labels'             => $labels,
            'supports'           => array( 'title', 'thumbnail' ),
            'public'             => true,
            'has_archive'        => true,
            'menu_position'      => 5,
            'menu_icon'          => 'dashicons-admin-multisite',
            'rewrite'            => array( 'slug' => 'industry' )
        );
        register_post_type( 'ot_industries', $args );
    };

?>