<?php

    // Enqueue assets
    add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
    function my_theme_enqueue_styles() {

        wp_enqueue_style( 'css-plugins', get_stylesheet_directory_uri() . '/vendor/css/plugins.min.css' );
        wp_enqueue_style( 'theme-info', get_stylesheet_directory_uri() . '/style.css' );
        wp_enqueue_style( 'theme-style', get_stylesheet_directory_uri() . '/assets/css/theme.css' );
        wp_enqueue_script( 'jquery','https://code.jquery.com/jquery-3.3.1.slim.min.js', array( 'jquery' ),'',true );
        wp_enqueue_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', array( 'jquery' ),'',true );
        wp_enqueue_script( 'easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', array( 'jquery' ),'',true );
        wp_enqueue_script( 'plugins-js', get_stylesheet_directory_uri() . '/vendor/js/plugins.min.js', array(), '', true );
        wp_enqueue_script( 'theme-js', get_stylesheet_directory_uri() . '/assets/js/theme.js', array(), '', true );

    }

    // Add feature image support
    add_theme_support( 'post-thumbnails' );

    // Primary menu
    function register_menu() {
        register_nav_menu('primary',__( 'Primary' ));
    }
    add_action( 'init', 'register_menu' );

    // Custom post types
    require get_template_directory() . '/inc/post-types.php';

    // Remove website field from comments
    function remove_website_field($arg) {
        $arg['url'] = '';
        return $arg;
    }
    add_filter('comment_form_default_fields', 'remove_website_field');

    // Customizer additions.
    require get_template_directory() . '/inc/customizer.php';

    // Add class to menu <li>
    function add_class_on_menu_li($classes, $item, $args) {
        if($args->add_li_class) {
            $classes[] = $args->add_li_class;
        }
        return $classes;
    }
    add_filter('nav_menu_css_class', 'add_class_on_menu_li', 1, 3);

    // Add class to menu <a>
    function add_class_on_menu_a( $atts, $item, $args ) {
        $class = $args->add_a_class;
        $atts['class'] = $class;
        return $atts;
    }
    add_filter( 'nav_menu_link_attributes', 'add_class_on_menu_a', 10, 3 );

?>