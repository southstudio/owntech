jQuery(fn = $ => {
    // Global URL
    // http://own-tech.com
    globarlUrl = 'http://own-tech.com';

    // Fade in function
    fadeIn = el => {
        el.classList.add('show');
        el.classList.remove('hide');  
    }
    
    // Fade out function
    fadeOut = el => {
        el.classList.add('hide');
        el.classList.remove('show');
    }

    // Display different logos depending on client scroll
    (navBarScroll = () => {
        var logoHeader = document.getElementById('logoHeader');
        var logoScroll = document.getElementById('logoScroll')
        if(window.pageYOffset > 100) {
            fadeOut(logoHeader);
            fadeIn(logoScroll);
            document.querySelector('.menu-wrapper').classList.add('menu-scroll');
        }
        else{
            fadeIn(logoHeader);
            fadeOut(logoScroll);
            document.querySelector('.menu-wrapper').classList.remove('menu-scroll');
        }
    })();

    // Collapse responsive navbar on click
    (collapseNavBar = () => {
        var navBarLinks = document.querySelectorAll('.navbar-collapse ul a');
        var toggler = document.querySelector('.navbar-toggler');
        if (window.innerWidth < 768) {
            navBarLinks.forEach(link => link.onclick = () => {
                // Trigger click on toggler when clicking on any link, if toggler is visible
                if (toggler.offsetWidth > 0 && toggler.offsetHeight > 0) { toggler.click(); }
            });

            $('#navbarTop').on('show.bs.collapse', () => {
                document.querySelector('.menu-wrapper').classList.add('menu-wrapper-opened');
            });

            $('#navbarTop').on('hide.bs.collapse', () => {
                document.querySelector('.menu-wrapper').classList.remove('menu-wrapper-opened');
            });
        }
    })();

    // Add active class to current menu item
    (activeClass = () => {
        document.getElementById('navbarTop').onclick = e => {
            if (document.querySelector('#navbarTop a.active') !== null) {
                document.querySelector('#navbarTop a.active').classList.remove('active');
            }
            e.target.classList.add('active');
        }
    })();

    // Back to top action
    (backToTop = () => {
        // Not yet supported on mobile
        /*var backToTop = document.getElementById('backToTop');
        backToTop.onclick = () => {
            window.scrollBy({
                top: 0,
                behavior: 'smooth'
            });
        }*/

        $('#backToTop').click(() => {
            $("html, body").animate({ scrollTop: 0 }, 'slow');
        });
    })();

    // Navbar behavior on link click
    (navBarBehavior = () => {
        $('.go-to-anchor').bind('click', function(event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: ($($anchor.attr('href')).offset().top)
            }, 1250, 'easeInOutExpo');
            event.preventDefault();
        });
    })();

    // Padding of sections with auto-scroll
    (scrollSectionHeight = () => {
        var navBarHeight = document.querySelector('.menu-wrapper').offsetHeight;
        document.querySelectorAll('.scroll-section').forEach(section => section.style.paddingTop = navBarHeight + 20 + 'px');
    })();

    // Carousel automatic indicators and active classes
    (carouselOptions = () => {
        // Add 'active' class to first carousel-item
        document.querySelectorAll('.ot-carousel').forEach(carousel => {
            carousel.querySelector('.carousel-item').classList.add('active');
        });
    })();

    // Different AOS delays to each different product
    (automaticAos = () => {
        var delay = 200;
        var delayIndustry = 200;
        document.querySelectorAll('.valores-container .automatic-aos').forEach(product => {
            product.setAttribute('data-aos-delay', delay);
            delay += 200;
        });
        document.querySelectorAll('.industrias-container .automatic-aos').forEach(product => {
            product.setAttribute('data-aos-delay', delayIndustry);
            delayIndustry += 200;
        });
    })();

    // Animate on scroll
    AOS.init();

    // Form file uploader
    (fileUploader = () => {
        var inputs = document.querySelectorAll( '.file-upload' );
        Array.prototype.forEach.call( inputs, input => {
            input.setAttribute('multiple', '');
            if (document.querySelector('.he-body') !== null) {
                input.setAttribute('data-multiple-caption', 'קבצים שנבחרו {count}');
            }
            else if (document.querySelector('.en-body') !== null) {
                input.setAttribute('data-multiple-caption', '{count} files selected');
            }
            else {
                input.setAttribute('data-multiple-caption', '{count} archivos seleccionados');
            }
            
            var label	 = document.querySelector('.file-group label'),
                labelVal = label.innerHTML;

            input.addEventListener( 'change', function(e) {
                var fileName = '';
                if( this.files && this.files.length > 1 ) {
                    fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                    var names = [];
                    Array.prototype.forEach.call( this.files, file => {
                        names.push(file.name);
                        var singleFile = document.createElement('div');
                        singleFile.classList.add('single-file');
                        singleFile.innerHTML = file.name;
                        document.querySelector('.file-names').appendChild(singleFile);
                    });
                }
                else {
                    document.querySelector('.file-names').innerHTML = '';
                    fileName = e.target.value.split( "\\" ).pop();
                };

                if( fileName ) {
                    document.querySelector('.file-group').querySelector( '.count' ).innerHTML = fileName;
                }
                else {
                    label.innerHTML = labelVal;
                };
            });
        });

        document.querySelector('.file-group label').onclick = () => {
            document.querySelector('input.file-upload').click();
        }
    })();

    (contactResponsive = () => {
        if (window.innerWidth < 992) {
            document.getElementById('contacto').style.marginTop = document.querySelector('#relaciones .ot-card').offsetHeight - 90 + 'px';
        }
        else{
            document.getElementById('contacto').style.marginTop = 0;
        }
    })();

    // Resize functions
    window.onresize = () => {
        collapseNavBar();
        scrollSectionHeight();
        contactResponsive();
    };

    window.addEventListener('scroll', () => {
        navBarScroll();
    });
});