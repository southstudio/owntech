# Custom WordPress theme, by South Studio

To kickstart this theme stylesheet, run `sass --watch assets/scss/theme.scss:style.css`.

## Bootstrap
To kickstart bootstrap, you will need to [download the latest bootstrap source files](https://getbootstrap.com/), copy the contents of its **scss** folder and paste them on `vendor/css/bootstrap`. Then run `sass --watch vendor/css/bootstrap/bootstrap.scss:vendor/css/bootstrap.css --style compressed`.
You will also need to copy the contents of its `js/bootstrap.min.js` and paste them on the theme's `js/app.min.js`.

This theme has support for:

* Thumbnails

* Primary menu

* Custom post type

* Modernizr 3.6.0 (touchevents)

* jQuery 3.3.1.slim

* Popper 1.14.7

* Aos plugin (animate on scroll)

* Back to top animation

* Logo uploader

* Social media icons

* Fontawesome 5

* Bootstrap menu

* Easing

* Anchor menu