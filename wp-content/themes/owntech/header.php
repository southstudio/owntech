<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?php echo get_bloginfo( 'name' );?>">
        <title><?php echo get_bloginfo( 'name' );?></title>
        <?php wp_head();?>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-154979357-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-154979357-1');
        </script>

    </head>
    <body class="<?php if (pll_current_language() == 'he') : ?>he-body<?php elseif (pll_current_language() == 'en') : ?>en-body<?php else : ?>es-body<?php endif ?>">

        <?php $GLOBALS['website_url'] = 'http://own-tech.com'; ?>
        
        <header class="d-flex flex-column">

            <div class="menu-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <nav class="navbar navbar-expand-md">
                                <?php if ( get_theme_mod( 'site_logo' ) || get_theme_mod( 'scroll_logo' ) ): ?>
                                    <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' )); ?>">
                                        <img id="logoHeader" class="img-fluid logo logo-header" src="<?php echo esc_attr(get_theme_mod( 'site_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                                        <img id="logoScroll" class="img-fluid logo logo-scroll hide" src="<?php echo esc_attr(get_theme_mod( 'scroll_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                                    </a>
                                <?php else : ?>
                                    <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' )); ?>"><?php esc_url(bloginfo('name')); ?></a>
                                <?php endif; ?>

                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTop" aria-controls="navbarTop" aria-expanded="false" aria-label="Toggle navigation">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </button>

                                <?php
                                    wp_nav_menu(array(
                                        'theme_location'  => 'primary',
                                        'container'       => 'div',
                                        'container_id'    => 'navbarTop',
                                        'container_class' => 'collapse navbar-collapse',
                                        'menu_class'      => 'navbar-nav ml-auto',
                                        'add_li_class'    => 'nav-item',
                                        'add_a_class'     => 'light-text regular-text fs-14 go-to-anchor'
                                    ));
                                ?>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container mt-auto">
                <div class="row">
                    <div class="col-12">
                        <div id="headerCarousel" class="carousel slide ot-carousel header-carousel" data-ride="carousel">
                            
                            <div class="carousel-inner-container">
                                <div class="carousel-inner">
                                    <?php
                                        $args = array(
                                            'post_type' => 'ot_header_slider'
                                        );
                                        $query_header_slider = new WP_Query($args);
                                        if ( $query_header_slider->have_posts() ) :
                                        while ( $query_header_slider->have_posts() ) : $query_header_slider->the_post();
                                    ?>
                                    <div class="carousel-item text-center text-md-left">
                                        <h1 class="light-text bold-text mxy-1 fs-40"><?php the_title(); ?></h1>
                                    </div>
                                    <?php 
                                        endwhile;
                                        wp_reset_postdata();
                                        endif;
                                    ?>
                                </div>
                            </div>
                            
                            <div class="controls-holder">
                                <a class="carousel-control carousel-control-prev" href="#headerCarousel" role="button" data-slide="prev">
                                    <i class="fas fa-long-arrow-alt-left light-text fs-30"></i>
                                </a>
                                <a class="carousel-control carousel-control-next" href="#headerCarousel" role="button" data-slide="next">
                                    <i class="fas fa-long-arrow-alt-right light-text fs-30"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </header>

        <div id="siteContent">