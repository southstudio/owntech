<?php get_header();?>

    <div class="site">

        <!-- US -->
        <div id="nosotros" class="nosotros-container scroll-section">
            <div class="container">
                <div class="ot-card blue-card <?php if (pll_current_language() == 'he') : ?>he-txt<?php endif ?>" data-aos="fade-up" data-aos-offset="0" data-aos-delay="300">
                    <?php if (pll_current_language() == 'es') : ?>
                        <h1 class="dark-text bold-text mxy-0 mb-30 fs-26">Nuestra Misión y Visión</h1>
                        <p class="dark-text regular-text mxy-0 fs-14">Consolidar un desarrollo sólido y diversificado de negocios internacionales que generen lazos comerciales y tecnológicos entre Argentina, Israel, América Latina y el resto del Mundo. Facilitando la integración, reduciendo costos y tiempos de inserción a nuevos mercados, optimizando beneficios para las partes intervinientes.</p>
                    <?php elseif (pll_current_language() == 'en') : ?>
                        <h1 class="dark-text bold-text mxy-0 mb-30 fs-26">Mission and Vision</h1>
                        <p class="dark-text regular-text mxy-0 fs-14">Consolidate a solid and diversified development of international businesses that generate commercial and technological ties between Argentina, Israel, Latin America and the rest of the World. Facilitating integration, reducing costs and insertion times to new markets, optimizing benefits for the parties involved.</p>
                    <?php else : ?>
                        <h1 class="dark-text bold-text mxy-0 mb-30 fs-26">משימה וחזון</h1>
                        <p class="dark-text regular-text mxy-0 fs-14">איחוד התפתחות מוצקה ומגוונת של עסקים בינלאומיים המייצרים קשרים מסחריים וטכנולוגיים בין ארגנטינה, ישראל, אמריקה הלטינית ושאר העולם. הקלת האינטגרציה, הפחתת עלויות וזמני הכניסה לשווקים חדשים, מיטוב מיטב היתרונות של הצדדים המעורבים.</p>
                    <?php endif ?>
                </div>
            </div>
        </div>

        <!-- VALUES -->
        <div id="valores" class="valores-container scroll-section">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1 class="dark-text bold-text fs-26 mxy-0 mb-70 <?php if (pll_current_language() == 'he') : ?>he-txt<?php endif ?>">
                            <?php if (pll_current_language() == 'es') : ?>
                                Nuestros valores
                            <?php elseif (pll_current_language() == 'en') : ?>
                                Our values
                            <?php else : ?>
                                הערכים שלנו
                            <?php endif ?>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="container grid-container">
                <?php
                    $args = array(
                        'post_type' => 'ot_values'
                    );
                    $query_values = new WP_Query($args);
                    if ( $query_values->have_posts() ) :
                    while ( $query_values->have_posts() ) : $query_values->the_post();
                ?>
                    <div class="ot-card white-card d-flex flex-column automatic-aos" data-aos="fade-up" data-aos-offset="200">
                        <h1 class="dark-text bold-text mxy-0 mb-20 pb-20 fs-20"><?php the_title(); ?></h1>
                        <?php the_content(); ?>
                        <div class="value-img text-center">
                            <?php the_post_thumbnail(); ?>
                        </div>
                    </div>
                <?php 
                    endwhile;
                    wp_reset_postdata();
                    endif;
                ?>
            </div>
        </div>

        <!-- TEAM -->
        <div id="equipo" class="team-container scroll-section">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1 class="dark-text bold-text fs-26 mxy-0 mb-70 <?php if (pll_current_language() == 'he') : ?>he-txt<?php endif ?>">
                            <?php if (pll_current_language() == 'es') : ?>
                                Nuestro equipo
                            <?php elseif (pll_current_language() == 'en') : ?>
                                Our team
                            <?php else : ?>
                                צוותנו
                            <?php endif ?>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="container grid-container">
                <?php
                    $args                               = array( 'post_type' => 'ot_team' );
                    $query_team                         = new WP_Query($args);
                    if ( $query_team->have_posts() )    :
                    while ( $query_team->have_posts() ) : $query_team->the_post();
                    $cargo                              = get_field( 'cargo' );
                    $linkedin                           = get_field( 'linkedin_url' );
                    $teamThumbnail                      = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                ?>
                    <div class="team-holder">
                        <div class="ot-card white-card p-0 team-image" style="background:url(<?php echo $teamThumbnail[0]; ?>) center center/cover no-repeat"></div>
                        <div class="team-body mt-30 pt-10">
                            <h1 class="dark-text bold-text mxy-0 fs-20"><?php the_title(); ?></h1>
                        </div>
                        <div class="team-footer mt-10">
                            <?php if( $linkedin ) : ?>
                                <a href="<?php echo $linkedin ?>" target="_blank" class="d-flex align-items-start <?php if (pll_current_language() == 'he') : ?>justify-content-end<?php endif ?>">
                                    <i class="fab fa-linkedin primary-text fs-20 mr-10 <?php if (pll_current_language() == 'he') : ?>order-2 mr-0 ml-10<?php endif ?>"></i>
                                    <?php if( $cargo ) : ?><p class="primary-text regular-text uppercase mxy-0 fs-14 <?php if (pll_current_language() == 'he') : ?>order-1<?php endif ?>"><?php echo $cargo; ?></p><?php endif; ?>
                                </a>
                            <?php else : ?>
                                <?php if( $cargo ) : ?><p class="primary-text regular-text uppercase mxy-0 fs-14"><?php echo $cargo; ?></p><?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php 
                    endwhile;
                    wp_reset_postdata();
                    endif;
                ?>
            </div>
        </div>

        <!-- HISTORY -->
        <div id="historia" class="historia-container scroll-section">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-4 col-xl-5 image-block p-static">
                        <div class="h-100"></div>
                    </div>
                    <div class="col-12 col-lg-8 col-xl-7">
                        <div class="ot-card blue-card" data-aos="fade-up" data-aos-delay="200">
                            <?php if (pll_current_language() == 'es') : ?>
                                <h1 class="dark-text bold-text mxy-0 mb-30 fs-26">Enfoque</h1>
                                <p class="dark-text regular-text mxy-0 fs-14">Con un mercado de más de 600 millones de habitantes que hablan un mismo idioma, e idiosincrasia similar, América Latina viene creciendo como destino diverso y atractivo para los inversionistas que desean expandirse globalmente y llevar su negocio al siguiente nivel.</p>
                                <p class="dark-text regular-text mxy-0 mb-10 fs-14">Formada por importantes países como Argentina, Brasil, Méjico, Chile, Perú y Colombia entre los más relevantes y muchos otros donde la población local tiene una educación superior, los gobiernos dan la bienvenida a la participación extranjera en una variedad de sectores.</p>
                                <p class="dark-text regular-text mxy-0 fs-14">Como toda región en vías de desarrollo necesita la incorporación de nuevas tecnologías y servicios en todos los sectores lo cual destaca como una excelente oportunidad de negocio.</p>
                                <p class="dark-text regular-text mxy-0 mb-10 fs-14">No obstante, la introducción en los mercados regionales es relativamente complicada para aquellos que no tienen el conocimiento de las normativas existentes, la lengua y la idiosincrasia por mencionar algunos de los principales factores.</p>
                                <p class="dark-text regular-text mxy-0 fs-14">Es por ello que nos hemos especializado en brindar servicios a firmas que quieran incorporarse a nuevos mercados y hoy contamos con un equipo especializado de personas que pueden ayudar a establecer y administrar su negocio con éxito en toda América Latina, optimizando sus ingresos y reduciendo costos.</p>
                            <?php elseif (pll_current_language() == 'en') : ?>
                                <h1 class="dark-text bold-text mxy-0 mb-30 fs-26">Focus</h1>
                                <p class="dark-text regular-text mxy-0 fs-14">With a market of more than 600 million inhabitants who speak the same language, and similar idiosyncrasy, Latin America has been growing as a diverse and attractive destination for investors who want to expand globally and take their business to the next level.</p>
                                <p class="dark-text regular-text mxy-0 mb-10 fs-14">Formed by important countries such as Argentina, Brazil, Mexico, Chile, Peru and Colombia among the most relevant and many others where the local population has a higher education, governments welcome foreign participation in a variety of sectors.</p>
                                <p class="dark-text regular-text mxy-0 fs-14">Like any developing region, it needs the incorporation of new technologies and services in all sectors, which stands out as an excellent business opportunity.</p>
                                <p class="dark-text regular-text mxy-0 mb-10 fs-14">However, the introduction into regional markets is relatively complicated for those who do not have knowledge of existing regulations, language and idiosyncrasy to mention some of the main factors.</p>
                                <p class="dark-text regular-text mxy-0 fs-14">That is why we have specialized in providing services to firms that want to join new markets and today we have a specialized team of people who can help establish and manage your business successfully throughout Latin America, optimizing your income and reducing costs.</p>
                            <?php else : ?>
                                <h1 class="dark-text bold-text mxy-0 mb-30 fs-26">פוקוס</h1>
                                <p class="dark-text regular-text mxy-0 fs-14">עם שוק של יותר מ -600 מיליון תושבים הדוברים אותה שפה, ודינוקרטיות דומה, אמריקה הלטינית צמחה כיעד מגוון ואטרקטיבי עבור משקיעים שרוצים להתרחב ברחבי העולם ולקחת את עסקיהם לשלב הבא</p>
                                <p class="dark-text regular-text mxy-0 mb-10 fs-14">נוצר על ידי מדינות חשובות כמו ארגנטינה, ברזיל, מקסיקו, צ'ילה, פרו וקולומביה בין הרלוונטיות ביותר ורבות אחרות בהן האוכלוסייה המקומית הינה בעלת השכלה גבוהה, ממשלות מברכות על השתתפות זרה במגוון מגזרים</p>
                                <p class="dark-text regular-text mxy-0 fs-14">כמו כל אזור מתפתח, הוא זקוק לשילוב טכנולוגיות ושירותים חדשים בכל המגזרים, אשר בולטת כהזדמנות עסקית מצוינת</p>
                                <p class="dark-text regular-text mxy-0 mb-10 fs-14">עם זאת, הכניסה לשווקים אזוריים מסובכת יחסית עבור מי שאין לו בקיאות בתקנות, בשפה ובאידיוסינקרטיה קיימת, להזכיר כמה מהגורמים העיקריים</p>
                                <p class="dark-text regular-text mxy-0 fs-14">לכן התמחינו במתן שירותים לחברות שרוצות להצטרף לשווקים חדשים וכיום יש לנו צוות מיוחד של אנשים שיכולים לעזור בהקמת וניהול העסק שלך בהצלחה ברחבי אמריקה הלטינית, מיטוב הכנסותיך והפחתת עלויות</p>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- DIVIDER -->
        <div class="divider d-none d-md-flex flex-column">
            <div class="container mt-auto mb-50">
                <div class="row">
                    <div class="col-12 text-center">
                        <img class="img-fluid mb-10" src="<?php echo esc_attr(get_theme_mod( 'site_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                    </div>
                </div>
            </div>
        </div>

        <!-- SERVICES -->
        <div id="servicios" class="servicios-container scroll-section">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center text-md-left">
                        <?php if (pll_current_language() == 'es') : ?>
                            <h1 class="dark-text bold-text mxy-0 mb-30 fs-26">Nuestros servicios</h1>
                        <?php elseif (pll_current_language() == 'en') : ?>
                            <h1 class="dark-text bold-text mxy-0 mb-30 fs-26">Our services</h1>
                        <?php else : ?>
                            <h1 class="dark-text bold-text mxy-0 mb-30 fs-26 he-txt">השירותים שלנו</h1>
                        <?php endif ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-3 col-xl-4"></div>
                    <div class="col-12 col-lg-9 col-xl-8">
                        <div class="row">
                            <div class="col-10 col-md-12 mx-auto">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <ul>
                                            <?php
                                                $args                                        = array(
                                                    'post_type' => 'ot_services',
                                                    'cat' => '34, 38, 41'
                                                );
                                                $query_services_left                         = new WP_Query($args);
                                                if ( $query_services_left->have_posts() )    :
                                                while ( $query_services_left->have_posts() ) : $query_services_left->the_post();
                                            ?>
                                                <li <?php if (pll_current_language() == 'he') : ?>class="he-txt"<?php endif; ?>><?php the_title(); ?></li>
                                            <?php
                                                endwhile;
                                                wp_reset_postdata();
                                                endif;
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-md-6 mt-15 mt-md-0">
                                        <ul>
                                            <?php
                                                $args                                         = array(
                                                    'post_type' => 'ot_services',
                                                    'cat' => '36, 49, 52'
                                                );
                                                $query_services_right                         = new WP_Query($args);
                                                if ( $query_services_right->have_posts() )    :
                                                while ( $query_services_right->have_posts() ) : $query_services_right->the_post();
                                            ?>
                                                <li <?php if (pll_current_language() == 'he') : ?>class="he-txt"<?php endif; ?>><?php the_title(); ?></li>
                                            <?php
                                                endwhile;
                                                wp_reset_postdata();
                                                endif;
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- INDUSTRIAS -->
        <div id="industrias" class="industrias-container scroll-section">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1 class="dark-text bold-text fs-26 mxy-0 mb-70 <?php if (pll_current_language() == 'he') : ?>he-txt<?php endif ?>">
                            <?php if (pll_current_language() == 'es') : ?>
                                Industrias
                            <?php elseif (pll_current_language() == 'en') : ?>
                                Industries
                            <?php else : ?>
                                תעשיות
                            <?php endif ?>
                        </h1>
                    </div>
                </div>
            </div>

            <?php
                $args              = array( 'post_type' => 'ot_industries' );
                $query_industries  = new WP_Query($args);
            ?>
                <div class="container grid-container">
                    <?php
                        if ( $query_industries->have_posts() )    :
                        while ( $query_industries->have_posts() ) : $query_industries->the_post();
                        $industriesThumbnail                      = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                    ?>
                        <div class="industry-holder automatic-aos" data-aos="fade-up">
                            <div class="ot-card white-card p-0 industry-image" style="background:url(<?php echo $industriesThumbnail[0]; ?>) center center/cover no-repeat"></div>
                            <div class="industry-body mt-30 pt-10">
                                <h1 class="dark-text bold-text mxy-0 fs-20"><?php the_title(); ?></h1>
                            </div>
                        </div>
                    <?php 
                        endwhile;
                        wp_reset_postdata();
                        endif;
                    ?>
                </div>

                <div class="container d-md-none">
                    <div class="row">
                        <div class="col-12">
                            <div id="industriesCarousel" class="carousel slide ot-carousel industries-carousel" data-ride="carousel">
                                <div class="carousel-inner-container">
                                    <div class="carousel-inner">
                                        <?php
                                            if ( $query_industries->have_posts() )    :
                                            while ( $query_industries->have_posts() ) : $query_industries->the_post();
                                            $industriesThumbnailSmall                 = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                                        ?>
                                            <div class="carousel-item">
                                                <div class="industry-holder">
                                                    <div class="ot-card white-card p-0 industry-image" style="background:url(<?php echo $industriesThumbnailSmall[0]; ?>) center center/cover no-repeat"></div>
                                                    <div class="industry-body mt-30 pt-10">
                                                        <h1 class="dark-text bold-text mxy-0 fs-20"><?php the_title(); ?></h1>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php 
                                            endwhile;
                                            wp_reset_postdata();
                                            endif;
                                        ?>
                                    </div>
                                </div>
                                
                                <a class="carousel-control carousel-control-prev" href="#industriesCarousel" role="button" data-slide="prev">
                                    <i class="fas fa-long-arrow-alt-left primary-text fs-30"></i>
                                </a>
                                <a class="carousel-control carousel-control-next" href="#industriesCarousel" role="button" data-slide="next">
                                    <i class="fas fa-long-arrow-alt-right primary-text fs-30"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php 
                wp_reset_postdata();
            ?>
        </div>

        <!-- RELACIONES -->
        <div id="relaciones" class="scroll-section">
            <div class="relaciones-container">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-lg-4 col-xl-5">
                            <div class="ot-card blue-card" data-aos="fade-up" data-aos-delay="200">
                                <?php if (pll_current_language() == 'es') : ?>
                                    <h1 class="dark-text bold-text mxy-0 mb-30 fs-26">Nuestras Relaciones</h1>
                                    <p class="dark-text regular-text mxy-0 mb-10 fs-14">Tenemos una gran cantidad de contactos claves en todos los países de Latino América, ya sea, del ámbito privado como público.</p>
                                    <p class="dark-text regular-text mxy-0 fs-14">Además, poseemos extensa experiencia en negocios. Logramos exitosamente generar puentes y vínculos entre los actores claves.</p>
                                <?php elseif (pll_current_language() == 'en') : ?>
                                    <h1 class="dark-text bold-text mxy-0 mb-30 fs-26">Our Relationships</h1>
                                    <p class="dark-text regular-text mxy-0 mb-10 fs-14">We have a large number of key contacts in all Latin American countries, whether from the private or public sphere.</p>
                                    <p class="dark-text regular-text mxy-0 fs-14">In addition, we have extensive business experience. We successfully generated bridges and links between the key actors.</p>
                                <?php else : ?>
                                    <h1 class="dark-text bold-text mxy-0 mb-30 fs-26">מערכות היחסים שלנו</h1>
                                    <p class="dark-text regular-text mxy-0 mb-10 fs-14">יש לנו מספר גדול של אנשי קשר מרכזיים בכל מדינות אמריקה הלטינית, בין אם הן פרטיות או ציבוריות</p>
                                    <p class="dark-text regular-text mxy-0 fs-14">בנוסף, יש לנו ניסיון עסקי נרחב. יצרנו בהצלחה גשרים וקשרים בין שחקני המפתח</p>
                                <?php endif ?>
                            </div>
                        </div>
                        <div class="col-12 col-lg-8 col-xl-7 image-block">
                            <div class="h-100"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="contacto" class="contacto-container scroll-section">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1 class="dark-text bold-text fs-26 mxy-0 mb-70 <?php if (pll_current_language() == 'he') : ?>he-txt<?php endif ?>">
                            <?php if (pll_current_language() == 'es') : ?>
                                Contacto
                            <?php elseif (pll_current_language() == 'en') : ?>
                                Contact
                            <?php else : ?>
                                יצירת קשר
                            <?php endif ?>
                        </h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="form-container">
                            <?php if (pll_current_language() == 'es') : ?>
                                <?php echo do_shortcode('[contact-form-7 id="227" title="Spanish contact form"]'); ?>
                            <?php elseif (pll_current_language() == 'en') : ?>
                                <?php echo do_shortcode('[contact-form-7 id="226" title="English contact form"]'); ?>
                            <?php else : ?>
                                <?php echo do_shortcode('[contact-form-7 id="228" title="Hebrew contact form"]'); ?>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

<?php get_footer();?>