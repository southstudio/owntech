        </div><!-- #siteContent -->
        
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-2 text-center text-lg-left">
                        <?php if ( get_theme_mod( 'site_logo' ) ): ?>
                            <img class="img-fluid logo logo-header" src="<?php echo esc_attr(get_theme_mod( 'site_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                        <?php else : ?>
                            <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' )); ?>"><?php esc_url(bloginfo('name')); ?></a>
                        <?php endif; ?>
                    </div>

                    <div class="col-12 col-lg-8 mt-20 mt-lg-0 mb-20 mb-lg-0">
                        <div class="row">
                            <div class="col-10 col-sm-8 col-md-6 col-lg-12 col-xl-10 mx-auto">
                                <div class="d-lg-flex align-items-start justify-content-between">
                                    <?php if ( get_theme_mod( 'tel' ) ): ?>
                                        <div class="d-flex align-items-start single-social light-text bold-text fs-14">
                                            <i class="fas fa-phone-alt"></i>
                                            <p class="mxy-0 ml-10"><?php echo get_theme_mod( 'tel' ); ?></p>
                                        </div>
                                    <?php endif; ?>
                                    <?php if ( get_theme_mod( 'email' ) ): ?>
                                        <div class="d-flex align-items-start single-social light-text bold-text fs-14">
                                            <i class="fas fa-envelope"></i>
                                            <p class="mxy-0 ml-10"><?php echo get_theme_mod( 'email' ); ?></p>
                                        </div>
                                    <?php endif; ?>
                                    <?php if ( get_theme_mod( 'address' ) ): ?>
                                        <div class="d-flex align-items-start single-social light-text bold-text fs-14">
                                            <i class="fas fa-map-marker-alt"></i>
                                            <p class="mxy-0 ml-10"><?php echo get_theme_mod( 'address' ); ?></p>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php if ( get_theme_mod( 'linkedin' ) ): ?>
                        <div class="col-12 col-lg-2">
                            <div class="row">
                                <div class="col-10 col-sm-8 col-md-6 col-lg-12 col-xl-10 mx-auto text-lg-right">
                                    <a href="<?php echo get_theme_mod( 'linkedin' ); ?>" class="light-text bold-text">
                                        <i class="fab fa-linkedin"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </footer>
        
        <?php wp_footer(); ?> 
    </body>
</html>